﻿// This file is part of Galaxy Editor.
//
// Galaxy Editor is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Galaxy Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Galaxy Editor. If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace GalaxyEditor
{
    public partial class GalaxyEditorForm : Form
    {
        public GalaxyEditorForm()
        {
            InitializeComponent();

            SuspendLayout();

            m_SectorInfo = new SectorInfo[24, 24];
            m_SectorButton = new SectorButton[24, 24];

            int buttonSize = 28;

            for (int x = 0; x < GalaxyInfo.NumSectorsX; ++x)
            {
                for (int y = 0; y < GalaxyInfo.NumSectorsY; ++y)
                {
                    m_SectorInfo[x, y] = new SectorInfo(x, y);

                    int positionX = propertyGrid.Location.X + propertyGrid.Width + 8 + (x % GalaxyInfo.NumSectorsX) * buttonSize;
                    int positionY = menuStrip1.Height + y * buttonSize;
                    m_SectorButton[x, y] = new SectorButton();
                    m_SectorButton[x, y].Text = "";
                    m_SectorButton[x, y].Location = new System.Drawing.Point(positionX, positionY);
                    m_SectorButton[x, y].Name = String.Format("Sector_{0}_{1}", x, y);
                    m_SectorButton[x, y].Size = new System.Drawing.Size(buttonSize, buttonSize);
                    m_SectorButton[x, y].TabIndex = x + y * GalaxyInfo.NumSectorsX;
                    m_SectorButton[x, y].UseVisualStyleBackColor = true;
                    m_SectorButton[x, y].SectorX = x;
                    m_SectorButton[x, y].SectorY = y;
                    m_SectorButton[x, y].Click += new System.EventHandler(sectorButton_Click);

                    Controls.Add(m_SectorButton[x, y]);
                }
            }

            ResumeLayout(false);
        }

        private void ReadXmlName(SectorInfo sectorInfo, XmlTextReader reader)
        {
            if (reader.Read() && reader.NodeType == XmlNodeType.Text)
            {
                sectorInfo.Name = reader.Value;                
            }
        }

        private void ReadXmlFaction(SectorInfo sectorInfo, XmlTextReader reader)
        {
            if (reader.Read() && reader.NodeType == XmlNodeType.Text)
            {
                sectorInfo.Faction = reader.Value;                
            }
        }

        private void ReadXmlShipyard(SectorInfo sectorInfo, XmlTextReader reader)
        {
            if (reader.Read() && reader.NodeType == XmlNodeType.Text)
            {
                sectorInfo.Shipyard = (reader.Value == "true");                
            }
        }

        private void ReadXmlBackgroundId(SectorInfo sectorInfo, XmlTextReader reader)
        {
            if (reader.Read() && reader.NodeType == XmlNodeType.Text)
            {
                sectorInfo.BackgroundId = Int32.Parse(reader.Value);                
            }
        }

        private void ReadXmlPersonal(SectorInfo sectorInfo, XmlTextReader reader)
        {
            if (reader.Read() && reader.NodeType == XmlNodeType.Text)
            {
                sectorInfo.Personal = (reader.Value == "true");                
            }
        }

        private void ReadXmlHyperspaceInhibitor(SectorInfo sectorInfo, XmlTextReader reader)
        {
            if (reader.Read() && reader.NodeType == XmlNodeType.Text)
            {
                sectorInfo.HyperspaceInhibitor = (reader.Value == "true");                
            }
        }

        private void ReadXmlHasStar(SectorInfo sectorInfo, XmlTextReader reader)
        {
            if (reader.Read() && reader.NodeType == XmlNodeType.Text)
            {
                sectorInfo.HasStar = (reader.Value == "true");                
            }
        }

        private void ReadXmlComponent(SectorInfo sectorInfo, XmlTextReader reader)
        {
            if (reader.Read() && reader.NodeType == XmlNodeType.Text)
            {
                sectorInfo.Components.Add(reader.Value);
            }
        }

        private void ReadXmlSector(XmlTextReader reader)
        {
            while (reader.Read() && reader.Name != "X");
            reader.Read();
            int x = Int32.Parse(reader.Value);
            while (reader.Read() && reader.Name != "Y");
            reader.Read();
            int y = Int32.Parse(reader.Value);

            SectorInfo sectorInfo = m_SectorInfo[x, y];
            SectorButton sectorButton = m_SectorButton[x, y];

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "Sector")
                {
                    break;
                }
                else
                {
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        if (reader.Name == "Faction")
                        {
                            ReadXmlFaction(sectorInfo, reader);
                        }
                        else if (reader.Name == "Name")
                        {
                            ReadXmlName(sectorInfo, reader);
                        }
                        else if (reader.Name == "Shipyard")
                        {
                            ReadXmlShipyard(sectorInfo, reader);
                        }
                        else if (reader.Name == "BackgroundId")
                        {
                            ReadXmlBackgroundId(sectorInfo, reader);
                        }
                        else if (reader.Name == "Personal")
                        {
                            ReadXmlPersonal(sectorInfo, reader);
                        }
                        else if (reader.Name == "HyperspaceInhibitor")
                        {
                            ReadXmlHyperspaceInhibitor(sectorInfo, reader);
                        }
                        else if (reader.Name == "HasStar")
                        {
                            ReadXmlHasStar(sectorInfo, reader);
                        }
                        else if (reader.Name == "Component")
                        {
                            ReadXmlComponent(sectorInfo, reader);
                        }
                    }
                }
            }
            
            SetupButton(sectorButton, sectorInfo);
        }

        private void SetupButton(SectorButton sectorButton, SectorInfo sectorInfo)
        {
            sectorButton.FlatStyle = FlatStyle.Flat;
            sectorButton.FlatAppearance.BorderSize = 1;

            if (sectorInfo.Faction == "Neutral")
            {
                sectorButton.BackColor = Color.Gray;
            }
            else if (sectorInfo.Faction == "Empire")
            {
                sectorButton.BackColor = Color.Blue;
            }
            else if (sectorInfo.Faction == "Marauders")
            {
                sectorButton.BackColor = Color.Red;
            }
            else if (sectorInfo.Faction == "Pirate")
            {
                sectorButton.BackColor = Color.Brown;
            }
            else if (sectorInfo.Faction == "Ascent")
            {
                sectorButton.BackColor = Color.Orange;
            }
            else if (sectorInfo.Faction == "Iriani")
            {
                sectorButton.BackColor = Color.Purple;
            }

            // Show the background ID unless there is a shipyard
            // There are so few shipyards it doesn't obscure visibility a great deal, but there isn't
            // enough space in a button to show both the ID and the S.
            sectorButton.Text = sectorInfo.Shipyard ? "S" : String.Format("{0}", sectorInfo.BackgroundId);
        }

        private void LoadGalaxy(String filename)
        {
            m_Filename = filename;

            if (System.IO.File.Exists(filename))
            {
                XmlTextReader reader = new XmlTextReader(filename);
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "Sector")
                    {
                        ReadXmlSector(reader);
                    }
                }
                reader.Close();
            }
        }

        private void SaveGalaxy()
        {
            if (m_Filename == null)
            {
                MessageBox.Show("Can't save galaxy before importing the sectors file.");
                return;
            }
                    
            XmlWriter writer = XmlWriter.Create(m_Filename);
            writer.WriteStartDocument();
            writer.WriteStartElement("Sectors");

            for (int x = 0; x < GalaxyInfo.NumSectorsX; ++x)
            {
                for (int y = 0; y < GalaxyInfo.NumSectorsY; ++y)
                {
                    SectorInfo sectorInfo = m_SectorInfo[x, y];

                    writer.WriteStartElement("Sector");

                    writer.WriteStartElement("X");
                    writer.WriteString(String.Format("{0}", x));
                    writer.WriteEndElement();

                    writer.WriteStartElement("Y");
                    writer.WriteString(String.Format("{0}", y));
                    writer.WriteEndElement();

                    writer.WriteStartElement("Name");
                    writer.WriteString(sectorInfo.Name);
                    writer.WriteEndElement();

                    writer.WriteStartElement("Faction");
                    writer.WriteString(sectorInfo.Faction);
                    writer.WriteEndElement();

                    writer.WriteStartElement("Shipyard");
                    writer.WriteString(sectorInfo.Shipyard ? "true" : "false");
                    writer.WriteEndElement();

                    writer.WriteStartElement("BackgroundId");
                    writer.WriteString(String.Format("{0}", sectorInfo.BackgroundId));
                    writer.WriteEndElement();

                    writer.WriteStartElement("Personal");
                    writer.WriteString(sectorInfo.Personal ? "true" : "false");
                    writer.WriteEndElement();

                    writer.WriteStartElement("HyperspaceInhibitor");
                    writer.WriteString(sectorInfo.HyperspaceInhibitor ? "true" : "false");
                    writer.WriteEndElement();

                    writer.WriteStartElement("HasStar");
                    writer.WriteString(sectorInfo.HasStar ? "true" : "false");
                    writer.WriteEndElement();

                    StringCollection components = sectorInfo.Components;
                    if (components.Count > 0)
                    {
                        writer.WriteStartElement("Components");

                        foreach (string component in components)
                        {
                            writer.WriteStartElement("Component");
                            writer.WriteString(component);
                            writer.WriteEndElement();
                        }

                        writer.WriteEndElement();
                    }

                    writer.WriteEndElement();
                }

            }

            writer.WriteEndElement();
            writer.WriteEndDocument();
            writer.Close();

            MessageBox.Show("Galaxy export completed.");
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                LoadGalaxy(openFileDialog.FileName);
            }            
        }

        private void exportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveGalaxy();
        }

        private void sectorButton_Click(object sender, EventArgs e)
        {
            SectorButton sectorButton = (SectorButton)sender;

            SectorInfo sectorInfo = m_SectorInfo[sectorButton.SectorX, sectorButton.SectorY];
            propertyGrid.SelectedObject = sectorInfo;
        }

        private void propertyGrid_Validated(object sender, EventArgs e)
        {
            if (propertyGrid.SelectedObject != null)
            {
                SectorInfo sectorInfo = (SectorInfo)propertyGrid.SelectedObject;
                SectorButton sectorButton = m_SectorButton[sectorInfo.X, sectorInfo.Y];
                SetupButton(sectorButton, sectorInfo);
            }
        }

        SectorInfo[,] m_SectorInfo;
        SectorButton[,] m_SectorButton;
        String m_Filename;
    }

    class GalaxyInfo
    {
        public static int NumSectorsX = 24;
        public static int NumSectorsY = 24;
    }

    class SectorInfo
    {
        public SectorInfo(int x, int y)
        {
            m_X = x;
            m_Y = y;
            m_Shipyard = false;
            m_Name = String.Format("{0} / {1}", x, y);
            m_BackgroundId = 0;
            m_Faction = "Neutral";
            m_Personal = false;
            m_HyperspaceInhibitor = false;
            m_HasStar = true;
            m_Components = new StringCollection();
        }

        public int X
        {
            get { return m_X; }
        }

        public int Y
        {
            get { return m_Y; }
        }

        public String Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        public int BackgroundId
        {
            get { return m_BackgroundId; }
            set { m_BackgroundId = value; }
        }

        public String Faction
        {
            get { return m_Faction; }
            set { m_Faction = value; }
        }

        public bool Shipyard
        {
            get { return m_Shipyard; }
            set { m_Shipyard = value; }
        }

        public bool Personal
        {
            get { return m_Personal; }
            set { m_Personal = value; }
        }

        public bool HyperspaceInhibitor
        {
            get { return m_HyperspaceInhibitor; }
            set { m_HyperspaceInhibitor = value; }
        }

        public bool HasStar
        {
            get { return m_HasStar; }
            set { m_HasStar = value; }
        }

        [Editor("System.Windows.Forms.Design.StringCollectionEditor,System.Design,Version=2.0.0.0,Culture=neutral,PublicKeyToken=b03f5f7f11d50a3a", typeof(System.Drawing.Design.UITypeEditor))]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public StringCollection Components
        {
            get { return m_Components; }
            set { m_Components = value; }
        }

        private int m_X;
        private int m_Y;
        private String m_Name;
        private int m_BackgroundId;
        private String m_Faction;
        private bool m_Shipyard;
        private bool m_Personal;
        private bool m_HyperspaceInhibitor;
        private bool m_HasStar;
        private StringCollection m_Components;
    }

    class SectorButton : Button
    {
        public int SectorX
        {
            get { return m_SectorX; }
            set { m_SectorX = value; }
        }

        public int SectorY
        {
            get { return m_SectorY; }
            set { m_SectorY = value; }
        }

        private int m_SectorX;
        private int m_SectorY;
    }
}
